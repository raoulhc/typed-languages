# Typed languages

Going through the book "Types and Programming Languages" by Benjamin Pierce, and
attempting to implement the languages in haskell. Hopefully getting some GADT
usage once I start to get onto typed languages.

There is no code generation for these languages and they're just examples to
play around with writing languages and seeing how they typecheck and evaluate.

Maybe one day I'll add some code generation backend to them to see how it works.
