module FullUntyped.AST where

import           Data.List                      ( intercalate )
import           Data.Data                      ( Data )
import qualified Data.Map.Strict                as M
import           Text.Printf                    ( printf )

import           FullUntyped.Position

type Name = String

-- | Context of what variables might represent
data Context
  = Context
  { lambdaBoundVariables :: ![Name]
  , exprBoundVariables :: !(M.Map Name (Expr Info))
  , freeVariables :: ![Name]
  } deriving (Eq, Data)

instance Show Context where
  show _ = "\b"

data Info
  = Info
  { infoctx :: !Context
  , infoloc :: !SrcSpan
  } deriving (Eq, Show, Data)

instance Spanned Info where
  getSpan = infoloc

newtype Block a = Block [Statement a] deriving (Eq, Show, Data)

data Statement a
  = StDeclaration !a !Name !(Expr a)
  | StExpr !a !(Expr a)
  deriving (Eq, Data)

-- | Different types of variables
data Var
  = LBound !Int  -- ^ Binding to an abstraction represented by distance to it
  | VBound !Name -- ^ Binding to a lambda expression done by name
  | FBound !Name -- ^ Binding to something not in scope, can't be evaluated
  deriving (Eq, Show, Data)

-- | The expression type, all contain an info type which gives the free variables
data Expr a
  = EVar !a !Var               -- ^ A variable with its de Bruijn value
  | EAbs !a !(Expr a)          -- ^ An abstraction with its enclosed expression
  | EApp !a !(Expr a) !(Expr a) -- ^ An application of one expression to another
  deriving (Eq, Data)

deriving instance Show a => Show (Statement a)

-- Instances

instance Spanned a => Spanned (Block a) where
  getSpan (Block sts) = spanDiff (head sts) (last sts)

instance GetAnnotation Statement where
  getAnnotation = \case
    StDeclaration a _ _ -> a
    StExpr a _          -> a

instance Spanned a => Spanned (Statement a) where
  getSpan = getSpan . getAnnotation

instance Functor Expr where
  fmap f (EVar a x    ) = EVar (f a) x
  fmap f (EAbs a e    ) = EAbs (f a) $ fmap f e
  fmap f (EApp a e1 e2) = EApp (f a) (fmap f e1) (fmap f e2)

instance Show a => Show (Expr a) where
  show = \case
    EVar a x     -> printf "(EVar (%s) %s)" (show a) (show x)
    EAbs a e     -> printf "(EAbs (%s) (%s))" (show a) (show e)
    EApp a e1 e2 -> printf "(EApp (%s) (%s) %s)" (show a) (show e1) (show e2)

-- | So we don't have to show the bound context variables when looking at info
instance {-# OVERLAPPING #-} Show (Expr Info) where
  show = \case
    EVar info x -> printf "(EVar (%s) %s)" (show $ infoloc info) (show x)
    EAbs info e -> printf "(EAbs (%s) (%s))" (show $ infoloc info) (show e)
    EApp info e1 e2 ->
      printf "(EApp (%s) (%s) %s)" (show $ infoloc info) (show e1) (show e2)

class GetAnnotation f where
  getAnnotation :: f a -> a

instance GetAnnotation Expr where
  getAnnotation = \case
    EVar info _   -> info
    EAbs info _   -> info
    EApp info _ _ -> info

instance Spanned a => Spanned (Expr a) where
  getSpan = getSpan . getAnnotation

class PrettyPrint a where
  pp :: a -> String

-- | Method to print expressions
instance PrettyPrint (Expr a) where
  pp (EVar _ var       ) = case var of
    LBound x -> show x
    VBound x -> x
    FBound x -> x
  pp (EAbs _ expr      ) = printf "(λ. %s)" $ pp expr
  pp (EApp _ expr expr') = printf "%s %s" (pp expr) (pp expr')

instance PrettyPrint (Statement a) where
  pp (StDeclaration _ name expr) = printf "%s = %s;" name $ pp expr
  pp (StExpr _ expr            ) = printf "%s;" $ pp expr

instance PrettyPrint [Statement a] where
  pp []         = ""
  pp (st : sts) = printf "%s\n%s" (pp st) (pp sts)

instance PrettyPrint (Block a ) where
  pp (Block sts) = intercalate "\n" $ fmap pp sts
