module FullUntyped.Position where

import           Data.Data                      ( Data )

newtype Location = Location (Int, Int)
  deriving (Eq, Data)

instance Show Location where
  show (Location (col, row)) = show col <> ":" <> show row

newtype SrcSpan = SrcSpan (Location, Location)
  deriving (Eq, Data)

instance Show SrcSpan where
  show (SrcSpan (start, end)) = show start <> "-" <> show end

class Spanned a where
  getSpan :: a -> SrcSpan

spanDiff :: (Spanned a, Spanned b) => a -> b -> SrcSpan
spanDiff s1 s2 =
  let SrcSpan (beg, _  ) = getSpan s1
      SrcSpan (_  , end) = getSpan s2
  in  SrcSpan (beg, end)

