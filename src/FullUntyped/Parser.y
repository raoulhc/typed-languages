-- -*- Mode: Hasell -*-
{
module FullUntyped.Parser
  ( parse
  ) where

import qualified Data.Map.Strict as M
import Control.Monad.State.Strict
import Data.Maybe (isJust)
import Data.List (elemIndex, findIndex, find)

import FullUntyped.Lexer
import FullUntyped.Position
import FullUntyped.AST
}

%name parse
%tokentype { Token }
%monad { State Context }
%error { parseError }

%token
lambda { TLambda pos }
'.' { TDot pos }
var { TVar pos s }
'(' { TLPar pos }
')' { TRPar pos }
';' { TSColon pos }
'=' { TEqual pos}
comment { THash pos s }

%left '.'

%%

-- | Entry to parser that allows
entry :: { [Statement Info] }
entry
: statement ';' entry
  {% do
      ctx <- get
      let info = Info ctx (spanDiff $1 $2)
      return $ ($1:$3)
  }
| statement ';'
  {% do
      ctx <- get
      let info = Info ctx (getSpan $1)
      return . pure $ $1
  }
| comment entry { $2 }

statement :: {Statement Info}
statement
: var '=' term
  {% do
      let TVar _ name = $1
      addVBndName name $3
      ctx <- get
      let info = Info ctx (spanDiff $1 $3)
      return $ StDeclaration info name $3
  }
| term
  {% do
      ctx <- get
      let info = Info ctx (getSpan $1)
      return $ StExpr info $1
  }


-- | (Currently) Top level entry to the parser
term :: { Expr Info }
term
: binding '.' term
  {% do
      ctx <- get
      let info = Info ctx (spanDiff $1 $3)
          expr = EAbs info $3
      rmLBndName
      return expr
  }
| appExpr { $1 }

-- | Application expressions, set up so ensure left association
appExpr :: { Expr Info }
appExpr
: aExpr { $1 }
| appExpr aExpr
  {% do
      ctx <- get
      let info = Info ctx $ spanDiff $1 $2
      return $ EApp info $1 $2
  }

-- | An atomic expression
aExpr :: { Expr Info }
aExpr
: '(' term ')'        { $2 }
| var
  {% do
      let TVar pos name = $1
      ix <- name2ix name
      ctx <- get
      let info = Info ctx (getSpan $1)
      return $ EVar info ix
  }

-- | Binds of a variable
binding :: { Token }
binding : lambda var
  {% do
      let t@(TVar pos name) = $2
      addLBndName name
      return t
  }

{
-- | Given a var token lookup, and maybe insert binding increment count
addLBndName :: String -> State Context ()
addLBndName name =
  modify (\(Context bnds exprbnds freebnds)
           -> Context (name:bnds) exprbnds freebnds)

addVBndName :: String -> Expr Info -> State Context ()
addVBndName name expr =
  modify (\(Context bnds exprbnds freebnds)
           -> Context bnds (M.insert name expr exprbnds) freebnds)

-- | remove a bound name when we go outside the parentheses
rmLBndName :: State Context ()
rmLBndName = modify (\(Context (b:lbnds) vbnds fbnds) -> Context lbnds vbnds fbnds)

-- | Get a variables index number, which may be a lambda bound, var bound or
-- free variable
name2ix :: String -> State Context Var
name2ix name = do
  Context lbnds vbnds fbnds <- get
  case elemIndex name lbnds of
    Just ix -> return $ LBound ix
    Nothing -> if M.member name vbnds
      then return $ VBound name
      else if name `elem` fbnds
        then return $ FBound name
        else do
          put $ Context lbnds vbnds (name:fbnds)
          return $ FBound name

-- | Error message
parseError :: [Token] -> a
parseError ts = error $ "Failed to parse" <> show ts
}
