-- -*- Mode: Haskell -*-
{
module FullUntyped.Lexer where

import FullUntyped.Position
}

%wrapper "posn"

tokens :-
  $white+	;
  \\         { \loc s -> TLambda (getspan loc s) }
  \.         { \loc s -> TDot (getspan loc s) }
  [A-Za-z']+ { \loc s -> TVar (getspan loc s) s }
  \(         { \loc s -> TLPar (getspan loc s) }
  \)         { \loc s -> TRPar (getspan loc s) }
  \;         { \loc s -> TSColon (getspan loc s) }
  \=         { \loc s -> TEqual (getspan loc s) }
  \#(.*)\n    { \loc s -> THash (getspan loc s) s}

{

getspan :: AlexPosn -> String -> SrcSpan
getspan (AlexPn _ line col) s =
  SrcSpan (Location (line, col), Location (line, col + length s))

data Token
  = TLambda SrcSpan
  | TDot SrcSpan
  | TLPar SrcSpan
  | TRPar SrcSpan
  | TVar SrcSpan String
  | TSColon SrcSpan
  | TEqual SrcSpan
  | THash SrcSpan String
  deriving (Eq, Show)

instance Spanned Token where
  getSpan = \case
    TLambda spn -> spn
    TDot spn -> spn
    TLPar spn -> spn
    TRPar spn -> spn
    TVar spn _ -> spn
    TSColon spn -> spn
    TEqual spn -> spn
    THash spn _ -> spn

}
