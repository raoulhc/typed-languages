module FullUntyped.Eval where

import           Control.Monad.Reader (Reader, ask)
import qualified Data.Map as M

import           FullUntyped.AST

-- Evalute expressions
eval :: Eq a => Expr a -> Reader (M.Map Name (Expr a)) (Expr a)
eval (EApp _ (EAbs _ t) v) = do
  v' <- eval v
  eval $ subs v' t
eval (EApp fi e1 e2) = do
  e1' <- eval e1
  e2' <- eval e2
  (if e1' /= e1 || e2' /= e2 then eval else return) (EApp fi e1' e2')
eval e@(EVar _ var) = case var of
  VBound name -> do
    ctx <- ask
    case M.lookup name ctx of
      Just expr -> eval expr
      Nothing -> error "Somehow had bound variable that didn't refer to anything"
  _ -> return e
eval e = return e

-- | Substitute expression cases
subs :: Eq a => Expr a -> Expr a -> Expr a
subs v t = shift (-1) (subs' 0 (shift 1 v) t)

-- | Top level shift
shift :: Eq a => Int -> Expr a -> Expr a
shift d = shift' d 0

-- | Helper expression to substitution case of a variblae with a new expression
subs'
  :: Eq a
  => Int    -- ^ Old variable to replace
  -> Expr a -- ^ New expression to substitute in
  -> Expr a -- ^ Old expression to transform
  -> Expr a -- ^ Resulting expression
subs' old new = exprMap
  (\fi c x -> if x == old + c then shift c new else EVar fi (LBound x))
  0

-- | Shift variable integers by a certain amount if they're above a cut off
-- amount
shift'
  :: Eq a
  => Int    -- ^ Amount to shift above
  -> Int    -- ^ Cut off, above which to shift
  -> Expr a -- ^ Old expression to transform
  -> Expr a -- ^ Resulting expression
shift' d = exprMap
  (\fi c' x -> if x >= c' then EVar fi (LBound $ x + d) else EVar fi (LBound x))

-- | Helper method that maps over expressions performing a function on vars and
-- increasing the nesting count everytime we descend into an abstraction
exprMap
  :: (a -> Int -> Int -> Expr a) -- ^ Method to apply on variables
  -> Int                         -- ^ Nesting count
  -> Expr a                      -- ^ Old expression
  -> Expr a                      -- ^ New expression
exprMap onvar c = \case
  EVar fi (LBound ix) -> onvar fi c ix
  e@EVar{}            -> e
  EAbs fi e           -> EAbs fi (exprMap onvar (c + 1) e)
  EApp fi e1 e2       -> EApp fi (exprMap onvar c e1) (exprMap onvar c e2)
