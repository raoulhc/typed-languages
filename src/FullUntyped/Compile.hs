module FullUntyped.Compile
  ( compile
  )
where

import qualified Data.Map.Strict as M
import           Control.Monad.Reader
import           Control.Monad.State.Strict

import           Options.Applicative

import           FullUntyped.Lexer
import           FullUntyped.Parser
import           FullUntyped.Eval
import           FullUntyped.AST

data Opt = Opt
  { file :: FilePath
  } deriving (Eq, Show)

optParser :: Parser Opt
optParser = Opt <$> argument str (metavar "FILE")

opts :: ParserInfo Opt
opts = info
  (optParser <**> helper)
  (fullDesc <> progDesc "Evaluate a lambda calculus expression" <> header "")

compile :: IO ()
compile = do
  popts    <- execParser opts

  -- Get tokens
  contents <- readFile $ file popts
  let tokens = alexScanTokens $ contents
  -- print tokens

  -- Parse into AST
  let ast      = parse tokens
      ctx      = Context [] M.empty []
      (res, ctx') = runState ast ctx
  -- print res

  -- Evaluate the last expression
  let StExpr _ lastexpr = last res
  let vbnds = exprBoundVariables ctx'
  putStrLn $ pp lastexpr
  putStrLn . pp $ runReader (eval lastexpr) vbnds
