-- -*- Mode: Haskell -*-
{
module Arith.Lexer where
}

%wrapper "basic"

tokens :-
  $white+	;
  true    { \_ -> TTrue }
  false   { \_ -> TFalse }
  if      { \_ -> TIf }
  then    { \_ -> TThen }
  else    { \_ -> TElse }
  0       { \_ -> TZero }
  succ    { \_ -> TSucc }
  pred    { \_ -> TPred }
  iszero  { \_ -> TIsZero }

{
data Token
  = TTrue
  | TFalse
  | TIf
  | TThen
  | TElse
  | TZero
  | TSucc
  | TPred
  | TIsZero
  deriving (Eq, Show)
}
