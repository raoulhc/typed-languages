-- -*- Mode: Hasell -*-
{
module Arith.Parser where

import Arith.Lexer

import Arith.AST
import Arith.Lexer
}

%name arith
%tokentype { Token }
%error { parseError }

%token
  true    { TTrue }
  false   { TFalse }
  if      { TIf }
  then    { TThen }
  else    { TElse }
  succ    { TSucc }
  pred    { TPred }
  zero    { TZero }
  iszero { TIsZero }

%%

Term : true { EVal VTrue }
     | false { EVal VFalse }
     | zero { EVal VZero }
     | if Term then Term else Term { EIf $2 $4 $6 }
     | succ Term { ESucc $2 }
     | pred Term { EPred $2 }
     | iszero Term { EIsZero $2 }

{
parseError :: [Token] -> a
parseError ts = error $ "Failed to parse" <> show ts
}
