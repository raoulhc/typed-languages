module Arith.Eval where

import           Arith.AST

eval :: Expr -> Value
eval (EVal v   ) = v
eval (EIf t u v) = case eval t of
  VTrue  -> eval u
  VFalse -> eval v
  _      -> error "Non boolean if expression"
eval (ESucc v) = case eval v of
  VZero   -> VSucc VZero
  VSucc x -> VSucc (VSucc x)
  _       -> error "Can't have successor of non number"
eval (EPred v) = case eval v of
  VSucc x -> x
  VZero   -> error "Can't use predecessor on zero"
  _       -> error "Can't have predecessor of non number"
eval (EIsZero v) = case eval v of
  VZero -> VTrue
  _     -> VFalse
