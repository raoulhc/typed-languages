module Arith.AST where

data Value
  = VTrue
  | VFalse
  | VZero
  | VSucc Value
  deriving (Show, Eq)

data Expr
  = EVal Value
  | EIf Expr Expr Expr
  | ESucc Expr
  | EPred Expr
  | EIsZero Expr
  deriving (Show, Eq)
