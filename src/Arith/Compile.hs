module Arith.Compile
  ( compile
  )
where

import           Arith.Lexer
import           Arith.Parser
import           Arith.Eval

compile :: IO ()
compile = getContents >>= print . eval . arith . alexScanTokens
